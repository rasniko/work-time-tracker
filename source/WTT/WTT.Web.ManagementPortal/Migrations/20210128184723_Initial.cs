﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WTT.Web.ManagementPortal.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Klijenti",
                columns: table => new
                {
                    IDKlijent = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Naziv = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Telefon = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Klijenti", x => x.IDKlijent);
                });

            migrationBuilder.CreateTable(
                name: "Timovi",
                columns: table => new
                {
                    IDTim = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Naziv = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DatumKreiranja = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Timovi", x => x.IDTim);
                });

            migrationBuilder.CreateTable(
                name: "TipoviDjelatnika",
                columns: table => new
                {
                    IDTipDjelatnika = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Naziv = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoviDjelatnika", x => x.IDTipDjelatnika);
                });

            migrationBuilder.CreateTable(
                name: "Djelatnici",
                columns: table => new
                {
                    IDDjelatnik = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ime = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Prezime = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DatumZaposlenja = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Zaporka = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TimID = table.Column<int>(type: "int", nullable: false),
                    TipDjelatnikaID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Djelatnici", x => x.IDDjelatnik);
                    table.ForeignKey(
                        name: "FK_Djelatnici_Timovi_TimID",
                        column: x => x.TimID,
                        principalTable: "Timovi",
                        principalColumn: "IDTim",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Djelatnici_TipoviDjelatnika_TipDjelatnikaID",
                        column: x => x.TipDjelatnikaID,
                        principalTable: "TipoviDjelatnika",
                        principalColumn: "IDTipDjelatnika",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Projekti",
                columns: table => new
                {
                    IDProjekt = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Naziv = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DatumOtvaranja = table.Column<DateTime>(type: "datetime2", nullable: false),
                    KlijentID = table.Column<int>(type: "int", nullable: false),
                    VoditeljProjektaID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projekti", x => x.IDProjekt);
                    table.ForeignKey(
                        name: "FK_Projekti_Djelatnici_VoditeljProjektaID",
                        column: x => x.VoditeljProjektaID,
                        principalTable: "Djelatnici",
                        principalColumn: "IDDjelatnik");
                    table.ForeignKey(
                        name: "FK_Projekti_Klijenti_KlijentID",
                        column: x => x.KlijentID,
                        principalTable: "Klijenti",
                        principalColumn: "IDKlijent",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DjelatnikProjekt",
                columns: table => new
                {
                    DjelatniciIDDjelatnik = table.Column<int>(type: "int", nullable: false),
                    ProjektiIDProjekt = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DjelatnikProjekt", x => new { x.DjelatniciIDDjelatnik, x.ProjektiIDProjekt });
                    table.ForeignKey(
                        name: "FK_DjelatnikProjekt_Djelatnici_DjelatniciIDDjelatnik",
                        column: x => x.DjelatniciIDDjelatnik,
                        principalTable: "Djelatnici",
                        principalColumn: "IDDjelatnik",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DjelatnikProjekt_Projekti_ProjektiIDProjekt",
                        column: x => x.ProjektiIDProjekt,
                        principalTable: "Projekti",
                        principalColumn: "IDProjekt",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Djelatnici_TimID",
                table: "Djelatnici",
                column: "TimID");

            migrationBuilder.CreateIndex(
                name: "IX_Djelatnici_TipDjelatnikaID",
                table: "Djelatnici",
                column: "TipDjelatnikaID");

            migrationBuilder.CreateIndex(
                name: "IX_DjelatnikProjekt_ProjektiIDProjekt",
                table: "DjelatnikProjekt",
                column: "ProjektiIDProjekt");

            migrationBuilder.CreateIndex(
                name: "IX_Projekti_KlijentID",
                table: "Projekti",
                column: "KlijentID");

            migrationBuilder.CreateIndex(
                name: "IX_Projekti_VoditeljProjektaID",
                table: "Projekti",
                column: "VoditeljProjektaID",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DjelatnikProjekt");

            migrationBuilder.DropTable(
                name: "Projekti");

            migrationBuilder.DropTable(
                name: "Djelatnici");

            migrationBuilder.DropTable(
                name: "Klijenti");

            migrationBuilder.DropTable(
                name: "Timovi");

            migrationBuilder.DropTable(
                name: "TipoviDjelatnika");
        }
    }
}
