﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WTT.Web.ManagementPortal.Migrations
{
    public partial class AddNapomena : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Napomena",
                table: "Evidencija",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Napomena",
                table: "Evidencija");
        }
    }
}
