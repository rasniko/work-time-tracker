﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WTT.Web.ManagementPortal.Migrations
{
    public partial class AddNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Djelatnici_Timovi_TimID",
                table: "Djelatnici");

            migrationBuilder.DropForeignKey(
                name: "FK_Djelatnici_TipoviDjelatnika_TipDjelatnikaID",
                table: "Djelatnici");

            migrationBuilder.AlterColumn<int>(
                name: "TipDjelatnikaID",
                table: "Djelatnici",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "TimID",
                table: "Djelatnici",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Djelatnici_Timovi_TimID",
                table: "Djelatnici",
                column: "TimID",
                principalTable: "Timovi",
                principalColumn: "IDTim",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Djelatnici_TipoviDjelatnika_TipDjelatnikaID",
                table: "Djelatnici",
                column: "TipDjelatnikaID",
                principalTable: "TipoviDjelatnika",
                principalColumn: "IDTipDjelatnika",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Djelatnici_Timovi_TimID",
                table: "Djelatnici");

            migrationBuilder.DropForeignKey(
                name: "FK_Djelatnici_TipoviDjelatnika_TipDjelatnikaID",
                table: "Djelatnici");

            migrationBuilder.AlterColumn<int>(
                name: "TipDjelatnikaID",
                table: "Djelatnici",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TimID",
                table: "Djelatnici",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Djelatnici_Timovi_TimID",
                table: "Djelatnici",
                column: "TimID",
                principalTable: "Timovi",
                principalColumn: "IDTim",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Djelatnici_TipoviDjelatnika_TipDjelatnikaID",
                table: "Djelatnici",
                column: "TipDjelatnikaID",
                principalTable: "TipoviDjelatnika",
                principalColumn: "IDTipDjelatnika",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
