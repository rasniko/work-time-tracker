﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WTT.Web.ManagementPortal.Migrations
{
    public partial class ConfigureMMT : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DjelatnikProjekt");

            migrationBuilder.CreateTable(
                name: "ProjektiDjelatnici",
                columns: table => new
                {
                    IDProjektDjelatnik = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DjelatnikID = table.Column<int>(type: "int", nullable: false),
                    ProjektID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjektiDjelatnici", x => x.IDProjektDjelatnik);
                    table.ForeignKey(
                        name: "FK_ProjektiDjelatnici_Djelatnici_DjelatnikID",
                        column: x => x.DjelatnikID,
                        principalTable: "Djelatnici",
                        principalColumn: "IDDjelatnik",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProjektiDjelatnici_Projekti_ProjektID",
                        column: x => x.ProjektID,
                        principalTable: "Projekti",
                        principalColumn: "IDProjekt",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProjektiDjelatnici_DjelatnikID",
                table: "ProjektiDjelatnici",
                column: "DjelatnikID");

            migrationBuilder.CreateIndex(
                name: "IX_ProjektiDjelatnici_ProjektID",
                table: "ProjektiDjelatnici",
                column: "ProjektID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProjektiDjelatnici");

            migrationBuilder.CreateTable(
                name: "DjelatnikProjekt",
                columns: table => new
                {
                    DjelatniciIDDjelatnik = table.Column<int>(type: "int", nullable: false),
                    ProjektiIDProjekt = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DjelatnikProjekt", x => new { x.DjelatniciIDDjelatnik, x.ProjektiIDProjekt });
                    table.ForeignKey(
                        name: "FK_DjelatnikProjekt_Djelatnici_DjelatniciIDDjelatnik",
                        column: x => x.DjelatniciIDDjelatnik,
                        principalTable: "Djelatnici",
                        principalColumn: "IDDjelatnik",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DjelatnikProjekt_Projekti_ProjektiIDProjekt",
                        column: x => x.ProjektiIDProjekt,
                        principalTable: "Projekti",
                        principalColumn: "IDProjekt",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DjelatnikProjekt_ProjektiIDProjekt",
                table: "DjelatnikProjekt",
                column: "ProjektiIDProjekt");
        }
    }
}
