﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WTT.Web.ManagementPortal.Migrations
{
    public partial class AdjustEvidencija : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Evidencija",
                columns: table => new
                {
                    IDEvidencija = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Datum = table.Column<DateTime>(type: "datetime2", nullable: false),
                    RedovanRad = table.Column<int>(type: "int", nullable: false),
                    PrekovremeniRad = table.Column<int>(type: "int", nullable: false),
                    ProjektID = table.Column<int>(type: "int", nullable: false),
                    DjelatnikID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Evidencija", x => x.IDEvidencija);
                    table.ForeignKey(
                        name: "FK_Evidencija_Djelatnici_DjelatnikID",
                        column: x => x.DjelatnikID,
                        principalTable: "Djelatnici",
                        principalColumn: "IDDjelatnik",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Evidencija_Projekti_ProjektID",
                        column: x => x.ProjektID,
                        principalTable: "Projekti",
                        principalColumn: "IDProjekt",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Evidencija_DjelatnikID",
                table: "Evidencija",
                column: "DjelatnikID");

            migrationBuilder.CreateIndex(
                name: "IX_Evidencija_ProjektID",
                table: "Evidencija",
                column: "ProjektID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Evidencija");
        }
    }
}
