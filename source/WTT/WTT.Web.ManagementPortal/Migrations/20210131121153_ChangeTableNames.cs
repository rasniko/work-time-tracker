﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WTT.Web.ManagementPortal.Migrations
{
    public partial class ChangeTableNames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Djelatnici_Timovi_TimID",
                table: "Djelatnici");

            migrationBuilder.DropForeignKey(
                name: "FK_Djelatnici_TipoviDjelatnika_TipDjelatnikaID",
                table: "Djelatnici");

            migrationBuilder.DropForeignKey(
                name: "FK_Evidencija_Djelatnici_DjelatnikID",
                table: "Evidencija");

            migrationBuilder.DropForeignKey(
                name: "FK_Evidencija_Projekti_ProjektID",
                table: "Evidencija");

            migrationBuilder.DropForeignKey(
                name: "FK_Projekti_Djelatnici_VoditeljProjektaID",
                table: "Projekti");

            migrationBuilder.DropForeignKey(
                name: "FK_Projekti_Klijenti_KlijentID",
                table: "Projekti");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjektiDjelatnici_Djelatnici_DjelatnikID",
                table: "ProjektiDjelatnici");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjektiDjelatnici_Projekti_ProjektID",
                table: "ProjektiDjelatnici");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TipoviDjelatnika",
                table: "TipoviDjelatnika");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Timovi",
                table: "Timovi");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProjektiDjelatnici",
                table: "ProjektiDjelatnici");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Projekti",
                table: "Projekti");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Klijenti",
                table: "Klijenti");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Djelatnici",
                table: "Djelatnici");

            migrationBuilder.RenameTable(
                name: "TipoviDjelatnika",
                newName: "TipDjelatnika");

            migrationBuilder.RenameTable(
                name: "Timovi",
                newName: "Tim");

            migrationBuilder.RenameTable(
                name: "ProjektiDjelatnici",
                newName: "ProjektDjelatnik");

            migrationBuilder.RenameTable(
                name: "Projekti",
                newName: "Projekt");

            migrationBuilder.RenameTable(
                name: "Klijenti",
                newName: "Klijent");

            migrationBuilder.RenameTable(
                name: "Djelatnici",
                newName: "Djelatnik");

            migrationBuilder.RenameIndex(
                name: "IX_ProjektiDjelatnici_ProjektID",
                table: "ProjektDjelatnik",
                newName: "IX_ProjektDjelatnik_ProjektID");

            migrationBuilder.RenameIndex(
                name: "IX_ProjektiDjelatnici_DjelatnikID",
                table: "ProjektDjelatnik",
                newName: "IX_ProjektDjelatnik_DjelatnikID");

            migrationBuilder.RenameIndex(
                name: "IX_Projekti_KlijentID",
                table: "Projekt",
                newName: "IX_Projekt_KlijentID");

            migrationBuilder.RenameIndex(
                name: "IX_Djelatnici_TipDjelatnikaID",
                table: "Djelatnik",
                newName: "IX_Djelatnik_TipDjelatnikaID");

            migrationBuilder.RenameIndex(
                name: "IX_Djelatnici_TimID",
                table: "Djelatnik",
                newName: "IX_Djelatnik_TimID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TipDjelatnika",
                table: "TipDjelatnika",
                column: "IDTipDjelatnika");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Tim",
                table: "Tim",
                column: "IDTim");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProjektDjelatnik",
                table: "ProjektDjelatnik",
                column: "IDProjektDjelatnik");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Projekt",
                table: "Projekt",
                column: "IDProjekt");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Klijent",
                table: "Klijent",
                column: "IDKlijent");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Djelatnik",
                table: "Djelatnik",
                column: "IDDjelatnik");

            migrationBuilder.AddForeignKey(
                name: "FK_Djelatnik_Tim_TimID",
                table: "Djelatnik",
                column: "TimID",
                principalTable: "Tim",
                principalColumn: "IDTim",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Djelatnik_TipDjelatnika_TipDjelatnikaID",
                table: "Djelatnik",
                column: "TipDjelatnikaID",
                principalTable: "TipDjelatnika",
                principalColumn: "IDTipDjelatnika",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Evidencija_Djelatnik_DjelatnikID",
                table: "Evidencija",
                column: "DjelatnikID",
                principalTable: "Djelatnik",
                principalColumn: "IDDjelatnik",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Evidencija_Projekt_ProjektID",
                table: "Evidencija",
                column: "ProjektID",
                principalTable: "Projekt",
                principalColumn: "IDProjekt",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Projekt_Djelatnik_VoditeljProjektaID",
                table: "Projekt",
                column: "VoditeljProjektaID",
                principalTable: "Djelatnik",
                principalColumn: "IDDjelatnik");

            migrationBuilder.AddForeignKey(
                name: "FK_Projekt_Klijent_KlijentID",
                table: "Projekt",
                column: "KlijentID",
                principalTable: "Klijent",
                principalColumn: "IDKlijent",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjektDjelatnik_Djelatnik_DjelatnikID",
                table: "ProjektDjelatnik",
                column: "DjelatnikID",
                principalTable: "Djelatnik",
                principalColumn: "IDDjelatnik",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjektDjelatnik_Projekt_ProjektID",
                table: "ProjektDjelatnik",
                column: "ProjektID",
                principalTable: "Projekt",
                principalColumn: "IDProjekt",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Djelatnik_Tim_TimID",
                table: "Djelatnik");

            migrationBuilder.DropForeignKey(
                name: "FK_Djelatnik_TipDjelatnika_TipDjelatnikaID",
                table: "Djelatnik");

            migrationBuilder.DropForeignKey(
                name: "FK_Evidencija_Djelatnik_DjelatnikID",
                table: "Evidencija");

            migrationBuilder.DropForeignKey(
                name: "FK_Evidencija_Projekt_ProjektID",
                table: "Evidencija");

            migrationBuilder.DropForeignKey(
                name: "FK_Projekt_Djelatnik_VoditeljProjektaID",
                table: "Projekt");

            migrationBuilder.DropForeignKey(
                name: "FK_Projekt_Klijent_KlijentID",
                table: "Projekt");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjektDjelatnik_Djelatnik_DjelatnikID",
                table: "ProjektDjelatnik");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjektDjelatnik_Projekt_ProjektID",
                table: "ProjektDjelatnik");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TipDjelatnika",
                table: "TipDjelatnika");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Tim",
                table: "Tim");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProjektDjelatnik",
                table: "ProjektDjelatnik");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Projekt",
                table: "Projekt");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Klijent",
                table: "Klijent");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Djelatnik",
                table: "Djelatnik");

            migrationBuilder.RenameTable(
                name: "TipDjelatnika",
                newName: "TipoviDjelatnika");

            migrationBuilder.RenameTable(
                name: "Tim",
                newName: "Timovi");

            migrationBuilder.RenameTable(
                name: "ProjektDjelatnik",
                newName: "ProjektiDjelatnici");

            migrationBuilder.RenameTable(
                name: "Projekt",
                newName: "Projekti");

            migrationBuilder.RenameTable(
                name: "Klijent",
                newName: "Klijenti");

            migrationBuilder.RenameTable(
                name: "Djelatnik",
                newName: "Djelatnici");

            migrationBuilder.RenameIndex(
                name: "IX_ProjektDjelatnik_ProjektID",
                table: "ProjektiDjelatnici",
                newName: "IX_ProjektiDjelatnici_ProjektID");

            migrationBuilder.RenameIndex(
                name: "IX_ProjektDjelatnik_DjelatnikID",
                table: "ProjektiDjelatnici",
                newName: "IX_ProjektiDjelatnici_DjelatnikID");

            migrationBuilder.RenameIndex(
                name: "IX_Projekt_KlijentID",
                table: "Projekti",
                newName: "IX_Projekti_KlijentID");

            migrationBuilder.RenameIndex(
                name: "IX_Djelatnik_TipDjelatnikaID",
                table: "Djelatnici",
                newName: "IX_Djelatnici_TipDjelatnikaID");

            migrationBuilder.RenameIndex(
                name: "IX_Djelatnik_TimID",
                table: "Djelatnici",
                newName: "IX_Djelatnici_TimID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TipoviDjelatnika",
                table: "TipoviDjelatnika",
                column: "IDTipDjelatnika");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Timovi",
                table: "Timovi",
                column: "IDTim");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProjektiDjelatnici",
                table: "ProjektiDjelatnici",
                column: "IDProjektDjelatnik");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Projekti",
                table: "Projekti",
                column: "IDProjekt");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Klijenti",
                table: "Klijenti",
                column: "IDKlijent");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Djelatnici",
                table: "Djelatnici",
                column: "IDDjelatnik");

            migrationBuilder.AddForeignKey(
                name: "FK_Djelatnici_Timovi_TimID",
                table: "Djelatnici",
                column: "TimID",
                principalTable: "Timovi",
                principalColumn: "IDTim",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Djelatnici_TipoviDjelatnika_TipDjelatnikaID",
                table: "Djelatnici",
                column: "TipDjelatnikaID",
                principalTable: "TipoviDjelatnika",
                principalColumn: "IDTipDjelatnika",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Evidencija_Djelatnici_DjelatnikID",
                table: "Evidencija",
                column: "DjelatnikID",
                principalTable: "Djelatnici",
                principalColumn: "IDDjelatnik",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Evidencija_Projekti_ProjektID",
                table: "Evidencija",
                column: "ProjektID",
                principalTable: "Projekti",
                principalColumn: "IDProjekt",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Projekti_Djelatnici_VoditeljProjektaID",
                table: "Projekti",
                column: "VoditeljProjektaID",
                principalTable: "Djelatnici",
                principalColumn: "IDDjelatnik");

            migrationBuilder.AddForeignKey(
                name: "FK_Projekti_Klijenti_KlijentID",
                table: "Projekti",
                column: "KlijentID",
                principalTable: "Klijenti",
                principalColumn: "IDKlijent",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjektiDjelatnici_Djelatnici_DjelatnikID",
                table: "ProjektiDjelatnici",
                column: "DjelatnikID",
                principalTable: "Djelatnici",
                principalColumn: "IDDjelatnik",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjektiDjelatnici_Projekti_ProjektID",
                table: "ProjektiDjelatnici",
                column: "ProjektID",
                principalTable: "Projekti",
                principalColumn: "IDProjekt",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
