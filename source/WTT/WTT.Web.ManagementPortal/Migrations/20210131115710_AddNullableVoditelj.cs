﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WTT.Web.ManagementPortal.Migrations
{
    public partial class AddNullableVoditelj : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Projekti_VoditeljProjektaID",
                table: "Projekti");

            migrationBuilder.AlterColumn<int>(
                name: "VoditeljProjektaID",
                table: "Projekti",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "VoditeljProjektaID",
                table: "Projekti",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }
    }
}
