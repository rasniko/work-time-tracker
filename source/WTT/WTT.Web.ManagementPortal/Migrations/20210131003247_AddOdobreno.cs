﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WTT.Web.ManagementPortal.Migrations
{
    public partial class AddOdobreno : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Odobreno",
                table: "Evidencija",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Odobreno",
                table: "Evidencija");
        }
    }
}
