﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using WTT.Common.Repositories;
using WTT.Common.Services;
using WTT.Web.ManagementPortal.Models;

namespace WTT.Web.ManagementPortal.Controllers
{
    [Authorize(Roles = "Direktor, Voditelj tima")]
    public class TimeManagementController : Controller
    {
        private readonly IDjelatnikRepository _djelatnikRepository;
        private readonly IProjectService _projectService;

        public TimeManagementController(IDjelatnikRepository djelatnikRepository, IProjectService projectService)
        {
            _djelatnikRepository = djelatnikRepository;
            _projectService = projectService;
        }

        [HttpGet]
        public async Task<IActionResult> OdobriVrijeme()
        {
            var djelatnikId = Convert.ToInt32(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var djelatnici = await _djelatnikRepository.GetDjelatnici(djelatnikId);

            var selectList = new List<SelectListItem>();
            foreach(var djelatnik in djelatnici)
            {
                selectList.Add(new SelectListItem(djelatnik.Name, djelatnik.DjelatnikId.ToString()));
            }

            ViewBag.SelectListDjelatnika = selectList;

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> OdobriVrijeme(OdobriVrijemeDTO odobriVrijeme)
        {
            var djelatnik = await _djelatnikRepository.GetDjelatnik(odobriVrijeme.DjelatnikId);
            var (evidencija, napomena) = await _projectService.GetProjectTimes(odobriVrijeme.DjelatnikId, odobriVrijeme.Datum);
            var timesDTO = new TimesDTO
            {
                DjelatnikId = odobriVrijeme.DjelatnikId,
                DjelatnikIme = $"{djelatnik.Ime} {djelatnik.Prezime}",
                DjelatnikTim = djelatnik.Tim.Naziv,
                Evidencija = evidencija,
                Datum = odobriVrijeme.Datum,
                Napomena = napomena
            };

            return View("Odobravanje", timesDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Odobravanje(TimesDTO timesDTO)
        {
            await _projectService.SaveProjectTimes(timesDTO.Evidencija, timesDTO.DjelatnikId, timesDTO.Datum, timesDTO.Napomena);

            return RedirectToAction("OdobriVrijeme");
        }

        [HttpGet]
        public async Task<IActionResult> GetProjects()
        {
            var projects = await _projectService.GetProjectTimes(1, DateTime.Today);

            return new JsonResult(projects);
        }
    }
}
