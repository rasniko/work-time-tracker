﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WTT.Web.ManagementPortal.Models;

namespace WTT.Web.ManagementPortal.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        private readonly Services.IAuthorizationService _authorizationService;

        public AccountController(Services.IAuthorizationService authorizationService)
        {
            _authorizationService = authorizationService;
        }

        [HttpGet]
        public IActionResult Login()
        {
            if (this.User.Identity.IsAuthenticated)
            {
                return RedirectToPage("/TimeTrackingPage");
            }

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginDTO loginModel)
        {
            if (ModelState.IsValid)
            {
                var result = await _authorizationService.SignInUserAsync(loginModel.Username, loginModel.Password);

                if (result)
                {
                    if (Request.Query.Keys.Contains("ReturnUrl"))
                    {
                        return Redirect(Request.Query["ReturnUrl"][0]);
                    }
                    else
                    {
                        return RedirectToPage("/TimeTrackingPage");
                    }
                }
            }

            ModelState.AddModelError("", "Failed to login");

            return View();
        }



        public async Task<IActionResult> Logout()
        {
            await _authorizationService.SignOutUserAsync();

            return RedirectToPage("/TimeTrackingPage");
        }
    }
}
