﻿namespace WTT.Web.ManagementPortal.Models
{
    public class LoginDTO
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
