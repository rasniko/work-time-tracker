﻿using System;
using System.Collections.Generic;
using WTT.Common.DTO;

namespace WTT.Web.ManagementPortal.Models
{
    public class TimesDTO
    {
        public string DjelatnikIme { get; set; }
        public int DjelatnikId { get; set; }
        public string DjelatnikTim { get; set; }

        public DateTime Datum { get; set; }

        public List<ProjectDTO> Evidencija { get; set; }

        public string Napomena { get; set; }
    }
}
