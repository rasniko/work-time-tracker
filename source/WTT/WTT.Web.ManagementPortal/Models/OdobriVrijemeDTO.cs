﻿using System;

namespace WTT.Web.ManagementPortal.Models
{
    public class OdobriVrijemeDTO
    {
        public int DjelatnikId { get; set; }
        public DateTime Datum { get; set; }
    }
}
