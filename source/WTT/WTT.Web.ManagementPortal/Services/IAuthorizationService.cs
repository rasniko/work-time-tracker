﻿using System.Threading.Tasks;

namespace WTT.Web.ManagementPortal.Services
{
    public interface IAuthorizationService
    {
        Task<bool> SignInUserAsync(string username, string password);
        Task SignOutUserAsync();
    }
}
