﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using WTT.Common.Repositories;

namespace WTT.Web.ManagementPortal.Services
{
    public class AuthorizationService : IAuthorizationService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IDjelatnikRepository _djelatnikRepository;

        public AuthorizationService(IHttpContextAccessor httpContextAccessor, IDjelatnikRepository djelatnikRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _djelatnikRepository = djelatnikRepository;
        }

        public async Task<bool> SignInUserAsync(string username, string password)
        {
            var djelatnik = await _djelatnikRepository.GetDjelatnik(username, password);
            if (djelatnik != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, username),
                    new Claim(ClaimTypes.NameIdentifier, djelatnik.IDDjelatnik.ToString()),
                    new Claim(ClaimTypes.Role, djelatnik.TipDjelatnika.Naziv)
                };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                await _httpContextAccessor.HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));

                return true;
            }

            return false;
        }

        public async Task SignOutUserAsync()
        {
            await _httpContextAccessor.HttpContext.SignOutAsync();
        }
    }
}
