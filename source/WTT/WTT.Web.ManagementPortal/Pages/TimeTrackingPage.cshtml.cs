﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WTT.Common.DTO;
using WTT.Common.Services;
using WTT.Web.ManagementPortal.Models;

namespace WTT.Web.ManagementPortal.Pages
{
    [IgnoreAntiforgeryToken(Order = 1001)]
    public class TimeTrackingPageModel : PageModel
    {
        private readonly IProjectService _projectService;

        public TimesDTO Evidencija { get; private set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime RequestedDate { get; set; }

        public TimeTrackingPageModel(IProjectService projectService)
        {
            _projectService = projectService;
        }

        public async Task OnGet()
        {
            RequestedDate = DateTime.Today;

            await OnPostWithDate(RequestedDate);
        }

        public async Task OnPostWithDate(DateTime requestedDate)
        {
            var djelantikId = Convert.ToInt32(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var (evidencija, napomena) = await _projectService.GetProjectTimes(djelantikId, requestedDate);

            Evidencija = new TimesDTO
            {
                DjelatnikId = djelantikId,
                Evidencija = evidencija,
                Datum = requestedDate,
                Napomena = napomena
            };
        }

        public async Task OnPost(TimesDTO evidencija)
        {
            if (evidencija?.Evidencija == null || evidencija.Evidencija.Count == 0)
            {
                return;
            }

            RequestedDate = evidencija.Datum;
            Evidencija = evidencija;

            var djelantikId = Convert.ToInt32(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);

            await _projectService.SaveProjectTimes(evidencija.Evidencija, djelantikId, RequestedDate, evidencija.Napomena);
        }
    }
}