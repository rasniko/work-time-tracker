﻿using Microsoft.EntityFrameworkCore;
using WTT.Data.EF6.Models;

namespace WTT.Data.EF6
{
    public class WttContext : DbContext
    {
        public DbSet<Djelatnik> Djelatnici { get; set; }
        public DbSet<Klijent> Klijenti { get; set; }
        public DbSet<Projekt> Projekti { get; set; }
        public DbSet<Tim> Timovi { get; set; }
        public DbSet<TipDjelatnika> TipoviDjelatnika { get; set; }
        public DbSet<ProjektiDjelatnici> ProjektiDjelatnici { get; set; }
        public DbSet<Evidencija> Evidencija { get; set; }

        public WttContext() : base()
        {

        }

        public WttContext(DbContextOptions<WttContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Projekt>()
                .HasOne(p => p.VoditeljProjekta)
                .WithOne(i => i.ProjektVoditelj)
                .HasForeignKey<Projekt>(b => b.VoditeljProjektaID)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
