﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WTT.Data.EF6.Models
{
    [Table("ProjektDjelatnik")]
    public class ProjektiDjelatnici
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int IDProjektDjelatnik { get; set; }

        public int DjelatnikID { get; set; }
        public Djelatnik Djelatnik { get; set; }

        public int ProjektID { get; set; }
        public Projekt Projekt { get; set; }
    }
}
