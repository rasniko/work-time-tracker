﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WTT.Data.EF6.Models
{
    [Table("Tim")]
    public class Tim
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int IDTim { get; set; }

        public string Naziv { get; set; }
        public DateTime DatumKreiranja { get; set; }

        public List<Djelatnik> Djelatnici { get; set; }
    }
}
