﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WTT.Data.EF6.Models
{
    [Table("Evidencija")]
    public class Evidencija
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int IDEvidencija { get; set; }

        public DateTime Datum { get; set; }
        public int RedovanRad { get; set; }
        public int PrekovremeniRad { get; set; }

        public bool Odobreno { get; set; }

        public Projekt Projekt { get; set; }
        public Djelatnik Djelatnik { get; set; }

        [ForeignKey("Projekt")]
        public int ProjektID { get; set; }

        [ForeignKey("Djelatnik")]
        public int DjelatnikID { get; set; }

        public string Napomena { get; set; }
    }
}
