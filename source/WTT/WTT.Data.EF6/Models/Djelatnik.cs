﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WTT.Data.EF6.Models
{
    [Table("Djelatnik")]
    public class Djelatnik
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int IDDjelatnik { get; set; }

        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Email { get; set; }
        public DateTime DatumZaposlenja { get; set; }
        public string Zaporka { get; set; }

        public Tim Tim { get; set; }
        public TipDjelatnika TipDjelatnika { get; set; }

        [InverseProperty("VoditeljProjekta")]
        public Projekt ProjektVoditelj { get; set; }

        public List<ProjektiDjelatnici> ProjektiDjelatnici { get; set; }

        [ForeignKey("Tim")]
        public int? TimID { get; set; }

        [ForeignKey("TipDjelatnika")]
        public int? TipDjelatnikaID { get; set; }
    }
}
