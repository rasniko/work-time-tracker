﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WTT.Data.EF6.Models
{
    [Table("Projekt")]
    public class Projekt
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int IDProjekt { get; set; }

        public string Naziv { get; set; }
        public DateTime DatumOtvaranja { get; set; }

        public Djelatnik VoditeljProjekta { get; set; }

        public List<ProjektiDjelatnici> ProjektiDjelatnici { get; set; }
        public Klijent Klijent { get; set; }

        [ForeignKey("Klijent")]
        public int KlijentID { get; set; }

        [ForeignKey("VoditeljProjekta")]
        public int? VoditeljProjektaID { get; set; }
    }
}
