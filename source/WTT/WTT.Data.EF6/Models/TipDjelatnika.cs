﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WTT.Data.EF6.Models
{
    [Table("TipDjelatnika")]
    public class TipDjelatnika
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int IDTipDjelatnika { get; set; }

        public string Naziv { get; set; }

        public List<Djelatnik> Djelatnici { get; set; }
    }
}
