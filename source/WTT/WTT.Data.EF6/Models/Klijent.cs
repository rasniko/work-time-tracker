﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WTT.Data.EF6.Models
{
    [Table("Klijent")]
    public class Klijent
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int IDKlijent { get; set; }

        public string Naziv { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }

        public List<Projekt> Projekti { get; set; }
    }
}
