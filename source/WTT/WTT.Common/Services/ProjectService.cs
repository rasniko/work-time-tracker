﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WTT.Common.DTO;
using WTT.Common.Repositories;
using WTT.Data.EF6.Models;

namespace WTT.Common.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IDjelatnikRepository _djelatnikRepository;

        public ProjectService(IDjelatnikRepository djelatnikRepository)
        {
            _djelatnikRepository = djelatnikRepository;
        }

        public async Task<(List<ProjectDTO>, string)> GetProjectTimes(int djelatnikId, DateTime datum)
        {
            var projects = new List<ProjectDTO>();
            var projekti = await _djelatnikRepository.GetDjelatnikProjekti(djelatnikId);
            var evidencija = await _djelatnikRepository.GetDjelatnikEvidencije(djelatnikId, datum);
            var napomena = "";

            foreach (var projekt in projekti)
            {
                var projektEvidencija = evidencija.Where(x => x.ProjektID == projekt.IDProjekt).FirstOrDefault();

                projects.Add(new ProjectDTO
                {
                    Id = projekt.IDProjekt,
                    Name = projekt.Naziv,
                    WorkMinutes = projektEvidencija != null ? projektEvidencija.RedovanRad : 0,
                    OverTimeMinutes = projektEvidencija != null ? projektEvidencija.PrekovremeniRad : 0,
                    Granted = projektEvidencija != null ? projektEvidencija.Odobreno : false
                });

                napomena = projektEvidencija != null ? projektEvidencija.Napomena : "";
            }

            return (projects, napomena);
        }

        public async Task SaveProjectTimes(List<ProjectDTO> projekti, int djelatnikId, DateTime datum, string napomena = "")
        {
            var evidencija = new List<Evidencija>();

            foreach (var projekt in projekti)
            {
                evidencija.Add(new Evidencija
                {
                    Datum = datum,
                    DjelatnikID = djelatnikId,
                    ProjektID = projekt.Id,
                    RedovanRad = projekt.WorkMinutes,
                    PrekovremeniRad = projekt.OverTimeMinutes,
                    Odobreno = projekt.Granted,
                    Napomena = napomena
                }) ;
            }

            await _djelatnikRepository.SaveDjelatnikEvidencije(evidencija, djelatnikId, datum);
        }
    }
}
