﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WTT.Common.DTO;

namespace WTT.Common.Services
{
    public interface IProjectService
    {
        Task<(List<ProjectDTO>, string)> GetProjectTimes(int djelatnikId, DateTime datum);

        Task SaveProjectTimes(List<ProjectDTO> projekti, int djelatnikId, DateTime datum, string napomena = "");
    }
}
