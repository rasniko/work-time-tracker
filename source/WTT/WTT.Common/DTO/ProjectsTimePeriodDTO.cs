﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WTT.Common.DTO
{
    public class ProjectsTimePeriodDTO
    {
        public DateTime Date { get; set; }
        public List<ProjectTimePeriodDTO> ProjectTimes { get; set; }
    }
}
