﻿namespace WTT.Common.DTO
{
    public class ProjectTimePeriodDTO
    {
        public int ProjektId { get; set; }
        public int MinutesWorked { get; set; }
    }
}
