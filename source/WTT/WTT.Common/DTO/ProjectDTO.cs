﻿using System;

namespace WTT.Common.DTO
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int WorkMinutes { get; set; }
        public int OverTimeMinutes { get; set; }

        public bool Granted { get; set; }
    }
}
