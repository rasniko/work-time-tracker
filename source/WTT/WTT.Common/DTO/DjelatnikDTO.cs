﻿namespace WTT.Common.DTO
{
    public class DjelatnikDTO
    {
        public int DjelatnikId { get; set; }
        public string Name { get; set; }
    }
}
