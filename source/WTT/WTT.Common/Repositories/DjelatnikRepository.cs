﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WTT.Common.DTO;
using WTT.Common.Enums;
using WTT.Data.EF6;
using WTT.Data.EF6.Models;

namespace WTT.Common.Repositories
{
    public class DjelatnikRepository : IDjelatnikRepository
    {
        private readonly WttContext _context;

        public DjelatnikRepository(WttContext context)
        {
            _context = context;
        }

        public async Task<Djelatnik> GetDjelatnik(string username, string password)
        {
            return await _context
                .Djelatnici
                .Include(x => x.TipDjelatnika)
                .Where(x => x.Email == username && x.Zaporka == password)
                .FirstOrDefaultAsync();
        }

        public async Task<List<Projekt>> GetDjelatnikProjekti(int djelatnikId)
        {
            var projekti = await _context.ProjektiDjelatnici.Where(x => x.DjelatnikID == djelatnikId).Select(x => x.ProjektID).ToListAsync();

            return await _context
                .Projekti
                .Where(x => projekti.Contains(x.IDProjekt))
                .ToListAsync();
        }

        public async Task<List<Evidencija>> GetDjelatnikEvidencije(int djelatnikId, DateTime datum)
        {
            return await _context
                .Evidencija
                .Where(x => x.DjelatnikID == djelatnikId && x.Datum.Date == datum.Date)
                .ToListAsync();
        }

        public async Task SaveDjelatnikEvidencije(List<Evidencija> evidencija, int djelatnikId, DateTime datum)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                _context.Evidencija.RemoveRange(_context.Evidencija.Where(x => x.DjelatnikID == djelatnikId && x.Datum.Date == datum.Date));
                await _context.AddRangeAsync(evidencija);

                await _context.SaveChangesAsync();
                await transaction.CommitAsync();
            }
        }

        public async Task<List<DjelatnikDTO>> GetDjelatnici(int djelatnikId)
        {
            var current = await GetDjelatnik(djelatnikId);
            if(current == null)
            {
                return new List<DjelatnikDTO>();
            }

            if(current.TipDjelatnikaID.GetValueOrDefault() == (int) TipDjelatnikaEnum.Direktor)
            {

                return await _context
                    .Djelatnici
                    .Where(x => x.TipDjelatnikaID.GetValueOrDefault() == (int)TipDjelatnikaEnum.VoditeljTima)
                    .Select(x => new DjelatnikDTO
                    {
                        DjelatnikId = x.IDDjelatnik,
                        Name = $"{x.Ime} {x.Prezime}"
                    })
                    .ToListAsync();
            }


            return await _context
                .Djelatnici
                .Where(x => x.TimID == current.TimID && x.IDDjelatnik != current.IDDjelatnik)
                .Select(x => new DjelatnikDTO 
                {  
                    DjelatnikId = x.IDDjelatnik,
                    Name = $"{x.Ime} {x.Prezime}"
                })
                .ToListAsync();
        }

        public async Task<Djelatnik> GetDjelatnik(int djelatnikId)
        {
            return await _context
                .Djelatnici
                .Include(x => x.Tim)
                .Where(x => x.IDDjelatnik == djelatnikId)
                .FirstOrDefaultAsync();
        }
    }
}
