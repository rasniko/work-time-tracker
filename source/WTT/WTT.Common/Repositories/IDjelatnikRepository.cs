﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WTT.Common.DTO;
using WTT.Data.EF6.Models;

namespace WTT.Common.Repositories
{
    public interface IDjelatnikRepository
    {
        Task<Djelatnik> GetDjelatnik(string username, string password);

        Task<List<Projekt>> GetDjelatnikProjekti(int djelatnikId);

        Task<List<Evidencija>> GetDjelatnikEvidencije(int djelatnikId, DateTime datum);

        Task SaveDjelatnikEvidencije(List<Evidencija> evidencija, int djelatnikId, DateTime datum);

        Task<List<DjelatnikDTO>> GetDjelatnici(int djelatnikId);

        Task<Djelatnik> GetDjelatnik(int djelatnikId);
    }
}
