﻿using System.ComponentModel;

namespace WTT.Common.Enums
{
    public enum TipDjelatnikaEnum
    {
        [Description("Direktor")]
        Direktor = 1,

        [Description("Voditelj tima")]
        VoditeljTima = 2,

        [Description("Zaposlenik")]
        Zaposlenik = 3,

        [Description("Honorarni djelatnik")]
        HonorarniDjelatnik = 4,

        [Description("Student")]
        Student = 5
    }
}
